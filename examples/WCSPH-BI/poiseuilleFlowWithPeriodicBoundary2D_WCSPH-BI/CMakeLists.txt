# add all targets
add_executable(poiseuilleFlowWithPeriodicBoundary2D_WCSPH-BI_cuda poiseuilleFlowWithPeriodicBoundary2D_WCSPH-BI.cu)

# add TNL to all targets
target_link_libraries(poiseuilleFlowWithPeriodicBoundary2D_WCSPH-BI_cuda PUBLIC TNL::TNL_CUDA)

set(TARGETS
        poiseuilleFlowWithPeriodicBoundary2D_WCSPH-BI_cuda
)

foreach(target IN ITEMS ${TARGETS})
    # add special flags
    target_compile_definitions(${target} PUBLIC "-DHAVE_CUDA")

    # enable OpenMP for the target
   find_package(OpenMP COMPONENTS CXX)
   if(OPENMP_FOUND)
        target_compile_definitions(${target} PUBLIC "-DHAVE_OPENMP")
        target_link_libraries(${target} PUBLIC OpenMP::OpenMP_CXX)
        # nvcc needs -Xcompiler
        target_compile_options(${target} PUBLIC $<$<CUDA_COMPILER_ID:NVIDIA>: -Xcompiler=-fopenmp >)
   endif()
endforeach()
