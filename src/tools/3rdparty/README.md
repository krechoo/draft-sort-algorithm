# Included third-party tools

### DualSPHysics GenCase

For particle discretisation of external geometry, at this point, GenCase by [DualSPHysics](https://dual.sphysics.org/) is used. This allows to process and generate initial particle distribution for complex geometries, which are used in several examples. The original can be found [in the DualSPHysics github repository](https://github.com/DualSPHysics/DualSPHysics/tree/master/bin/linux). For further details about GenCase, visit [DualSPHysics webpage](https://dual.sphysics.org/).
